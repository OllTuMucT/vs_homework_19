#include <iostream>
#include <ctime>
#define DEBUG
using std::cout;
using std::endl;
#ifdef DEBUG
	#define _CRTDBG_MAP_ALLOC
	_CrtMemState s1, s2, s3;
#endif

class Animal
{
public:
	virtual void Voice(){}
	virtual ~Animal(){}
};

class Dog:public Animal
{
public:
	void Voice() override
	{
		cout << "Woof!" << endl;
	}
	~Dog() {}
};

class Cat:public Animal
{
public:
	void Voice() override
	{
		cout << "Mee-ow!" << endl;
	}
	~Cat() {}
};

class Pig:public Animal
{
public:
	void Voice() override
	{
		cout << "Oink!" << endl;
	}
	~Pig() {}
};

class Sheep :public Animal
{
public:
	void Voice() override
	{
		cout << "Baa!" << endl;
	}
	~Sheep() {}
};

class Cow :public Animal
{
public:
	void Voice() override
	{
		cout << "Mooooo!" << endl;
	}
	~Cow() {}
};

int main()
{
	_CrtMemCheckpoint(&s1);
	srand(time(NULL));
	int n = 20;
	unsigned short int RandomValue = 0;
	Animal**p= new Animal*[n];
	for (int i = 0; i < n; i++)
	{
		RandomValue = rand() % 5 + 1;
		switch(RandomValue)
		{
		case 1:
			*(p + i)= new Dog();
			break;
		case 2:
			*(p + i) = new Cat();
			break;
		case 3:
			*(p + i) = new Pig();
			break;
		case 4:
			*(p + i) = new Sheep();
			break;
		case 5:
			*(p + i) = new Cow();
			break;
		}
		p[i]->Voice();
		delete p[i];
	}
	delete [] p;

#ifdef DEBUG
	_CrtMemCheckpoint(&s2);
	if (_CrtMemDifference(&s3, &s1, &s2))
		_CrtMemDumpStatistics(&s3);
#endif
}